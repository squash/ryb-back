package com.example.server.juc;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

/**
 * xxx
 *
 * @author zcg
 * @since 2021/9/6
 */
public class Test01Interrupter {

    @Test
    public void test01() throws InterruptedException {
        TwoPhaseTermination twoPhaseTermination = new TwoPhaseTermination();
        twoPhaseTermination.start();

        Thread.sleep(3500);
        // 打断monitor线程
        twoPhaseTermination.stop();
    }
}

// 两阶段终止模式，用于优雅的停止线程
@Slf4j
class TwoPhaseTermination {
    private Thread monitor;

    public void start() {
        monitor = new Thread(() -> {
            while (true) {
                // 判断是否被打断
                Thread current = Thread.currentThread();
                if (current.isInterrupted()) {
                    // 被打断了，料理后事并打断
                    System.out.println("料理后事");
                    break;
                }
                // 睡眠2秒
                try {
                    Thread.sleep(1000);
                    System.out.println("执行监控记录");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    // 出现异常重新设置，打断标记
                    current.interrupt();
                }
            }

        });
        monitor.start();
    }

    public void stop() {
        monitor.interrupt();
    }

}
