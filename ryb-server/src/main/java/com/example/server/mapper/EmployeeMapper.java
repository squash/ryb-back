package com.example.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.server.pojo.Employee;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {

    IPage<Employee> selectEmployeeByPage(Page<Employee> page
            , @Param("employee") Employee employee
            , @Param("beginDateScope") LocalDate[] beginDateScope);

    List<Employee> selectEmployees(Integer id);

    IPage<Employee> selectEmployeeWithSalary(Page<Employee> page);
}
