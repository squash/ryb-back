package com.example.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.server.pojo.MailLog;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
@Mapper
public interface MailLogMapper extends BaseMapper<MailLog> {

}
