package com.example.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.server.pojo.PoliticsStatus;

/**
 * xxx
 *
 * @author zcg
 * @since 2021/9/17
 */
public interface PoliticsStatusMapper extends BaseMapper<PoliticsStatus> {

}

