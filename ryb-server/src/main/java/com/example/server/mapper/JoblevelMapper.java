package com.example.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.server.pojo.Joblevel;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
public interface JoblevelMapper extends BaseMapper<Joblevel> {

}
