package com.example.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.server.pojo.MenuRole;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
@Mapper
public interface MenuRoleMapper extends BaseMapper<MenuRole> {


    /**
     * 更新角色的菜单
     *
     * @param rid  角色id
     * @param mids 菜单id数组
     * @return 影响的条数
     */
    int insertMidsByRid(@Param("rid") Integer rid, @Param("mids") Integer[] mids);
}
