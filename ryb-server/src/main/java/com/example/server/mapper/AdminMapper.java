package com.example.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.server.pojo.Admin;
import com.example.server.pojo.Role;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
@Mapper
public interface AdminMapper extends BaseMapper<Admin> {

    List<Role> getRoles(@Param("adminId") Integer adminId);

    List<Admin> selectAllAdmins(@Param("adminId") Integer adminId, @Param("keywords") String keywords);

    int addRoles(@Param("adminId") Integer adminId, @Param("rids") Integer[] rids);
}
