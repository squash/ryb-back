package com.example.server.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.server.pojo.Department;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
@Mapper
public interface DepartmentMapper extends BaseMapper<Department> {

    List<Department> selectAllDepartments(Integer parentId);

    void insertDept(Department department);

    void deleteDept(Department department);
}
