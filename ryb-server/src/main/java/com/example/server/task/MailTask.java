package com.example.server.task;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.server.pojo.Employee;
import com.example.server.pojo.MailConstants;
import com.example.server.pojo.MailLog;
import com.example.server.service.IEmployeeService;
import com.example.server.service.IMailLogService;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.List;

/**
 * 邮件发送定时任务
 *
 * @author zcg
 * @since 2021/9/19
 */
@Component
public class MailTask {

    @Autowired
    private IMailLogService mailLogService;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private IEmployeeService employeeService;

    /**
     * 邮件发送定时任务
     * 10秒一次
     */
    @Scheduled(cron = "0/10 * * * * ?")
    public void sendMailOnFail() {
        // 状态为0投递中并且重试时间小于当前时间的才需要重新发送
        List<MailLog> mailLogs = mailLogService.list(new LambdaQueryWrapper<MailLog>().eq(MailLog::getStatus, 0)
                .lt(MailLog::getTryTime, LocalDateTime.now()));
        mailLogs.forEach(mailLog -> {
            // 重试次数超过3次，更新为消息投递失败
            if (mailLog.getCount() >= 3) {
                mailLogService.update(new LambdaUpdateWrapper<MailLog>()
                        .set(MailLog::getStatus, 2)
                        .eq(MailLog::getMsgId, mailLog.getMsgId()));
            }
            // 否则更新重试次数，更新时间，重试时间
            mailLogService.update(new UpdateWrapper<MailLog>()
                    .set("count", mailLog.getCount() + 1)
                    .set("updateTime", LocalDateTime.now())
                    .set("tryTime", LocalDateTime.now().plusMinutes(MailConstants.MSG_TIMEOUT))
                    .eq("msgId", mailLog.getMsgId()));
            // 获取重新发送的消息内容
            Employee employee = employeeService.findEmployees(mailLog.getEid()).get(0);

            // 重新发送消息
            rabbitTemplate.convertAndSend(MailConstants.MAIL_EXCHANGE_NAME
                    , MailConstants.MAIL_ROUTING_KEY_NAME
                    , employee, new CorrelationData(mailLog.getMsgId()));
        });
    }
}
