package com.example.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.server.pojo.Department;
import com.example.server.pojo.RespBean;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
public interface IDepartmentService extends IService<Department> {

    /**
     * 获取所有部门
     *
     * @return List<Department>
     */
    List<Department> getAllDepartments();

    /**
     * 添加部门
     *
     * @param department 部门实体
     */
    void addDept(Department department);

    /**
     * 删除部门
     *
     * @param id 部门id
     * @return 通用返回实体
     */
    RespBean deleteDept(Integer id);
}
