package com.example.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.server.pojo.MenuRole;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
public interface IMenuRoleService extends IService<MenuRole> {

    /**
     * 更新角色的菜单
     *
     * @param rid  角色id
     * @param mids 菜单id数组
     * @return 更新影响的条数
     */
    int updateMenuRole(Integer rid, Integer[] mids);
}
