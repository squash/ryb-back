package com.example.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.server.pojo.PoliticsStatus;

/**
 * xxx
 *
 * @author zcg
 * @since 2021/9/17
 */
public interface IPoliticsStatusService extends IService<PoliticsStatus> {

}

