package com.example.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.server.pojo.Admin;
import com.example.server.pojo.RespBean;
import com.example.server.pojo.Role;
import org.springframework.security.core.Authentication;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
public interface IAdminService extends IService<Admin> {

    /**
     * 登录之后返回token
     *
     * @param username 用户名
     * @param password 密码
     * @param request  请求对象
     * @return RespBean
     */
    RespBean login(String username, String password, String captcha, HttpServletRequest request);

    /**
     * 根据用户名获取当前登录用户信息
     *
     * @param username 用户名
     * @return Admin
     */
    Admin getAdminByUserName(String username);

    /**
     * 获取当前管理员的角色列表
     *
     * @param adminId 用户id
     * @return 角色列表
     */
    List<Role> getRoles(Integer adminId);


    /**
     * 获取所有操作员
     *
     * @param keywords 关键词，模糊搜索
     * @return 操作员列表
     */
    List<Admin> getAllAdmins(String keywords);

    /**
     * 更新操作员角色
     *
     * @param adminId 操作id
     * @param rids    角色ids
     * @return 通用返回实体
     */
    RespBean updateAdminRole(Integer adminId, Integer[] rids);

    /**
     * 更新管理员密码
     *
     * @param oldPass 原密码
     * @param pass    新密码
     * @param adminId 管理员id
     * @return RespBean
     */
    RespBean updatePassword(String oldPass, String pass, Integer adminId);

    /**
     * 更新管理员头像
     *
     * @param url            头像地址
     * @param id             管理员id
     * @param authentication security authentication
     * @return RespBean
     */
    RespBean updateAdminUserFace(String url, Integer id, Authentication authentication);
}
