package com.example.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.server.mapper.PoliticsStatusMapper;
import com.example.server.pojo.PoliticsStatus;
import com.example.server.service.IPoliticsStatusService;
import org.springframework.stereotype.Service;

/**
 * xxx
 *
 * @author zcg
 * @since 2021/9/17
 */
@Service
public class PoliticsStatusServiceImpl extends ServiceImpl<PoliticsStatusMapper, PoliticsStatus> implements IPoliticsStatusService {

}
