package com.example.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.server.pojo.SysMsgContent;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
public interface ISysMsgContentService extends IService<SysMsgContent> {

}
