package com.example.server.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.server.mapper.DepartmentMapper;
import com.example.server.pojo.Department;
import com.example.server.pojo.RespBean;
import com.example.server.service.IDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

    @Autowired
    private DepartmentMapper departmentMapper;

    @Override
    public List<Department> getAllDepartments() {
        return departmentMapper.selectAllDepartments(-1);
    }

    @Override
    public void addDept(Department department) {
        department.setEnabled(true);
        departmentMapper.insertDept(department);
    }

    @Override
    public RespBean deleteDept(Integer id) {
        Department department = new Department();
        department.setId(id);

        departmentMapper.deleteDept(department);
        Integer result = department.getResult();
        if (result == -2) {
            return RespBean.error("该部门下有子部门，删除失败！");
        } else if (result == -1) {
            return RespBean.error("该部门下有员工，删除失败！");
        } else if (result == 1) {
            return RespBean.success("删除成功");
        }
        return RespBean.error("删除失败");
    }
}
