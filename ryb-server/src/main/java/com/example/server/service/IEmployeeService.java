package com.example.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.server.pojo.Employee;
import com.example.server.pojo.RespBean;
import com.example.server.pojo.RespPageBean;

import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
public interface IEmployeeService extends IService<Employee> {

    RespPageBean findEmployeeByPage(Integer currentPage, Integer size, Employee employee, LocalDate[] beginDateScope);

    RespBean addEmployee(Employee employee);

    /**
     * 条件查询员工
     *
     * @param id 员工id，存在则查询单个员工的资料，否则查询全部员工的资料
     * @return 员工信息列表/单个员工信息
     */
    List<Employee> findEmployees(Integer id);

    /**
     * 获取员工账套
     *
     * @param currentPage 当前页
     * @param size        每页条数
     * @return 分页对象
     */
    RespPageBean findEmployeeWithSalary(Integer currentPage, Integer size);
}
