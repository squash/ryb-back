package com.example.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.server.pojo.Menu;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
public interface IMenuService extends IService<Menu> {

    /**
     * 通过用户id查询用户菜单
     *
     * @return List<Menu>
     */
    List<Menu> getMenusByAdminId();

    /**
     * 根据角色查询菜单列表
     *
     * @return List<Menu>
     */
    List<Menu> getMenusWithRole();

    /**
     * 查询所有菜单，树状形式
     *
     * @return 菜单列表
     */
    List<Menu> getAllMenus();
}
