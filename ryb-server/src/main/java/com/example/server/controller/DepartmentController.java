package com.example.server.controller;


import com.example.server.pojo.Department;
import com.example.server.pojo.RespBean;
import com.example.server.service.IDepartmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
@RestController
@RequestMapping("/system/basic/department")
@Api(tags = "部门管理")
public class DepartmentController {

    @Autowired
    private IDepartmentService departmentService;

    @GetMapping("/")
    @ApiOperation(value = "获取所有部门")
    public RespBean getAllDepartments() {
        return RespBean.result(departmentService.getAllDepartments());
    }

    @PostMapping("/")
    @ApiOperation(value = "添加部门")
    public RespBean addDept(@RequestBody Department department) {
        departmentService.addDept(department);
        if (department.getResult() == 1) {
            return RespBean.success("添加部门成功", department);
        }
        return RespBean.error("添加部门失败");
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "删除部门")
    public RespBean deleteDept(@PathVariable Integer id) {
        return departmentService.deleteDept(id);
    }
}
