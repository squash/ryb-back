package com.example.server.controller;


import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import cn.afterturn.easypoi.excel.entity.enmus.ExcelType;
import com.example.server.pojo.*;
import com.example.server.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.time.LocalDate;
import java.util.List;

/**
 * <p>
 * 员工
 * </p>
 *
 * @author zcg
 * @since 2021-09-03
 */
@RestController
@RequestMapping("/employee/basic")
@Api(tags = "员工管理")
public class EmployeeController {

    @Autowired
    private IEmployeeService employeeService;

    @Autowired
    private IPositionService positionService;

    @Autowired
    private IPoliticsStatusService politicsStatusService;

    @Autowired
    private IJoblevelService joblevelService;

    @Autowired
    private INationService nationService;

    @Autowired
    private IDepartmentService departmentService;

    @ApiOperation(value = "获取所有员工")
    @GetMapping("/")
    public RespPageBean getEmployee(@RequestParam(defaultValue = "1") Integer currentPage
            , @RequestParam(defaultValue = "10") Integer size
            , Employee employee, LocalDate[] beginDateScope) {
        return employeeService.findEmployeeByPage(currentPage, size, employee, beginDateScope);
    }

    @ApiOperation(value = "添加员工")
    @PostMapping("/")
    public RespBean addEmp(@RequestBody Employee employee) {
        return employeeService.addEmployee(employee);
    }

    @ApiOperation(value = "更新员工")
    @PutMapping("/")
    public RespBean updateEmp(@RequestBody Employee employee) {
        if (employeeService.updateById(employee)) {
            return RespBean.success("更新成功");
        }
        return RespBean.error("更新失败");
    }

    @ApiOperation(value = "删除员工")
    @DeleteMapping("/{id}")
    public RespBean deleteEmp(@PathVariable Integer id) {
        if (employeeService.removeById(id)) {
            return RespBean.success("删除成功");
        }
        return RespBean.error("删除失败");
    }

    @ApiOperation(value = "员工导出")
    @GetMapping(value = "/export", produces = "application/octet-stream")
    public void exportEmployees(HttpServletResponse response) {
        List<Employee> employees = employeeService.findEmployees(null);
        // 创建导出参数，HSSF代表excel类型为.xls
        ExportParams exportParams = new ExportParams("员工表", "员工表", ExcelType.HSSF);
        // 创建工作薄对象
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, Employee.class, employees);
        // 以流的方式输出到浏览器
        ServletOutputStream out = null;
        try {
            response.setHeader("content-type", "application/octet-stream");
            response.setHeader("content-disposition", "attachment;filename=" + URLEncoder.encode("员工表.xls", "utf-8"));
            out = response.getOutputStream();
            workbook.write(out);
        } catch (IOException e) {
            if (out != null) {
                try {
                    out.close();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
    }

    @ApiOperation(value = "员工导入")
    @ApiImplicitParams({@ApiImplicitParam(name = "file", value = "上传文件", dataType = "MultipartFile")})
    @PostMapping(value = "/import")
    public RespBean importEmployees(MultipartFile file) {
        ImportParams importParams = new ImportParams();
        // 去掉标题行
        importParams.setTitleRows(1);
        List<Nation> nations = nationService.list();
        List<PoliticsStatus> politicsStatuses = politicsStatusService.list();
        List<Department> departments = departmentService.list();
        List<Position> positions = positionService.list();
        List<Joblevel> jobLevels = joblevelService.list();
        try {
            List<Employee> employees = ExcelImportUtil.importExcel(file.getInputStream(), Employee.class, importParams);
            employees.forEach(employee -> {
                // 民族id
                int nationId = nations.get(nations.indexOf(new Nation(employee.getNation().getName()))).getId();
                // 政治面貌id
                int politicsStatusId = politicsStatuses.get(politicsStatuses.indexOf(new PoliticsStatus(employee.getPoliticsStatus().getName()))).getId();
                // 部门id
                Integer departmentId = departments.get(departments.indexOf(new Department(employee.getDepartment().getName()))).getId();
                // 职位id
                Integer positionId = positions.get(positions.indexOf(new Position(employee.getPosition().getName()))).getId();
                // 职称id
                Integer jobLevelId = jobLevels.get(jobLevels.indexOf(new Joblevel(employee.getJoblevel().getName()))).getId();

                employee.setNationId(nationId);
                employee.setPoliticId(politicsStatusId);
                employee.setDepartmentId(departmentId);
                employee.setPosId(positionId);
                employee.setJobLevelId(jobLevelId);
            });
            boolean res = employeeService.saveBatch(employees);
            if (res) {
                return RespBean.success("操作成功");
            }
            return RespBean.error("导入失败");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return RespBean.success("操作失败");
    }

    @ApiOperation(value = "获取所有政治面貌")
    @GetMapping("/politicsStatus")
    private RespBean getAllPoliticsStatus() {
        return RespBean.result(politicsStatusService.list());
    }

    @ApiOperation(value = "获取所有职称")
    @GetMapping("/joblevel")
    public RespBean getAllJobLevel() {
        return RespBean.result(joblevelService.list());
    }

    @ApiOperation(value = "获取所有民族")
    @GetMapping("/nation")
    public RespBean getAllNation() {
        return RespBean.result(nationService.list());
    }

    @ApiOperation(value = "获取所有职位")
    @GetMapping("/position")
    public RespBean getAllPosition() {
        return RespBean.result(positionService.list());
    }

    @ApiOperation(value = "获取所有部门")
    @GetMapping("/deps")
    public RespBean getAllDepartment() {
        return RespBean.result(departmentService.getAllDepartments());
    }
}
