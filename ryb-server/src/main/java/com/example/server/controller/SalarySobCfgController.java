package com.example.server.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.example.server.pojo.Employee;
import com.example.server.pojo.RespBean;
import com.example.server.pojo.RespPageBean;
import com.example.server.pojo.Salary;
import com.example.server.service.IEmployeeService;
import com.example.server.service.ISalaryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 工资账套设置
 *
 * @author zcg
 * @since 2021/9/20
 */
@RequestMapping("/salary/sobcfg")
@Api(tags = "工资账套设置")
@RestController
public class SalarySobCfgController {

    @Autowired
    private ISalaryService salaryService;

    @Autowired
    private IEmployeeService employeeService;

    @GetMapping("/salaries")
    @ApiOperation("获取所有工资账套")
    public List<Salary> getAllSalaryCfg() {
        return salaryService.list();
    }

    @GetMapping("/")
    @ApiOperation("获取所有员工账套")
    public RespPageBean getEmployeeWithSalary(@RequestParam(defaultValue = "1") Integer currentPage
            , @RequestParam(defaultValue = "10") Integer size) {
        return employeeService.findEmployeeWithSalary(currentPage, size);
    }

    @ApiOperation(value = "更新员工套账")
    @PutMapping("/")
    public RespBean updateEmployeeSalary(@ApiParam(value = "员工id") Integer eid
            , @ApiParam(value = "工资账套id") Integer sid) {
        if (employeeService.update(new UpdateWrapper<Employee>().set("salaryId", sid).eq("id", eid))) {
            return RespBean.success("更新成功");
        }
        return RespBean.error("更新失败");
    }
}
