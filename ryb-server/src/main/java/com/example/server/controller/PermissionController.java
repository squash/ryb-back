package com.example.server.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.example.server.pojo.MenuRole;
import com.example.server.pojo.RespBean;
import com.example.server.pojo.Role;
import com.example.server.service.IMenuRoleService;
import com.example.server.service.IMenuService;
import com.example.server.service.IRoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 权限组
 *
 * @author zcg
 * @since 2021/9/12
 */
@RestController
@RequestMapping("/system/basic/permission")
@Api(tags = "权限组管理")
public class PermissionController {

    @Autowired
    private IRoleService roleService;

    @Autowired
    private IMenuService menuService;

    @Autowired
    private IMenuRoleService menuRoleService;

    @ApiOperation(value = "查询所有角色")
    @GetMapping("/")
    public RespBean getAllRoles() {
        return RespBean.result(roleService.list());
    }

    @ApiOperation(value = "添加角色")
    @PostMapping("/role")
    public RespBean addRole(@RequestBody Role role) {
        if (!role.getName().startsWith("ROLE_")) {
            role.setName("ROLE_" + role.getName());
        }
        if (roleService.save(role)) {
            return RespBean.success("添加成功");
        }
        return RespBean.error("添加失败");
    }

    @ApiOperation(value = "删除角色")
    @DeleteMapping("/role/{rid}")
    public RespBean deleteRole(@PathVariable Integer rid) {
        if (roleService.removeById(rid)) {
            return RespBean.success("删除成功");
        }
        return RespBean.error("删除失败");
    }

    @ApiOperation(value = "查询所有菜单")
    @GetMapping("/menus")
    public RespBean getAllMenus() {
        return RespBean.result(menuService.getAllMenus());
    }

    @ApiOperation(value = "根据角色id获取菜单id")
    @GetMapping("mid/{rid}")
    public RespBean getMidByRid(@PathVariable Integer rid) {
        List<Integer> midList = menuRoleService.list(new LambdaQueryWrapper<MenuRole>().eq(MenuRole::getRid, rid))
                .stream()
                .map(MenuRole::getMid)
                .collect(Collectors.toList());
        return RespBean.result(midList);
    }

    @ApiOperation(value = "更新权限组菜单")
    @PutMapping("/")
    public RespBean updateMenuRole(Integer rid, Integer[] mids) {
        int count = menuRoleService.updateMenuRole(rid, mids);
        if (mids == null || count == mids.length) {
            return RespBean.success("更新成功");
        }
        return RespBean.error("更新失败");
    }
}
