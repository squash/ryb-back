package com.example.server.controller;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * xxx
 *
 * @author zcg
 * @since 2021/9/5
 */
@RestController
@Api(tags = "测试")
public class HelloWorldController {

    @PostMapping("/hello")
    public String hello(){
        return "hello";
    }

    @GetMapping("/employee/base/hello")
    public String hello2(){
        return "/employee/base/hello";
    }

    @GetMapping("/employee/advanced/hello")
    public String hello3(){
        return "/employee/advanced/hello";
    }
}
