package com.example.server.exception;

/**
 * 自定义异常类，应用异常
 *
 * @author zcg
 * @since 2021/9/9
 */
public class AppException extends RuntimeException {

    public AppException() {

    }
    public AppException(String msg) {
        super(msg);
    }
}
