package com.example.server.util;

import com.example.server.pojo.Admin;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 * xxx
 *
 * @author zcg
 * @since 2021/9/14
 */
public class AppUtil {

    public static Admin currentAdmin() {
        return (Admin) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
