package com.example.server.config;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.example.server.pojo.MailConstants;
import com.example.server.pojo.MailLog;
import com.example.server.service.IMailLogService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * rabbit mq配置
 *
 * @author zcg
 * @since 2021/9/19
 */
@Configuration
@Slf4j
public class RabbitMQConfig {

    @Autowired
    private CachingConnectionFactory connectionFactory;

    @Autowired
    private IMailLogService mailLogService;

    @Bean
    public RabbitTemplate rabbitTemplate() {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);

        // 消息确认回调，确认消息是否到达broker
        rabbitTemplate.setConfirmCallback((data, ack, cause) -> {
            if (data != null) {
                String msgId = data.getId();
                if (ack) {
                    // 确认成功
                    log.info("消息发送成功==========>{}", msgId);
                    mailLogService.update(new LambdaUpdateWrapper<MailLog>()
                            .set(MailLog::getStatus, 1).eq(MailLog::getMsgId, msgId));
                } else {
                    log.error("消息发送失败===========>{}", msgId);
                }
            }
            log.error("消息发送失败===========>{}", ack);
        });

        // 消息失败回调，比如router不到queue时回调
        rabbitTemplate.setReturnCallback((msg, repCode, repText, exchange, routingKey) -> {
            log.error("消息发送到queue失败===========>{}", msg.getBody());
        });
        return rabbitTemplate;
    }

    @Bean
    public Queue queue() {
        return new Queue(MailConstants.MAIL_QUEUE_NAME);
    }

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange(MailConstants.MAIL_EXCHANGE_NAME);
    }

    @Bean
    public Binding binding() {
        return BindingBuilder.bind(queue())
                .to(directExchange())
                .with(MailConstants.MAIL_ROUTING_KEY_NAME);
    }
}
