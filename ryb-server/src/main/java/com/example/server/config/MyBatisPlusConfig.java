package com.example.server.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.BlockAttackInnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.InnerInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * MyBatis Plus配置
 *
 * @author zcg
 * @since 2021/9/16
 */
@Configuration
public class MyBatisPlusConfig {
    // 最新版
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        List<InnerInterceptor> list = new ArrayList<>();
        // 防止全表更新与删除拦截器
        list.add(new BlockAttackInnerInterceptor());
        // 增加分页拦截器，否则mp分页不生效
        list.add(new PaginationInnerInterceptor(DbType.MYSQL));
        interceptor.setInterceptors(list);
        return interceptor;
    }
}
