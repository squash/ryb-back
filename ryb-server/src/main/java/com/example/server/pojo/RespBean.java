package com.example.server.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p>
 * 通用返回对象
 * </p>
 *
 * @author zcg
 * @since 2021/9/3
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RespBean {

    private Integer code;
    private String message;
    private Object data;

    public static RespBean success(String message) {
        return new RespBean(200, message, null);
    }

    public static RespBean result(Object data) {
        return new RespBean(200, "操作成功", data);
    }

    public static RespBean success(String message, Object data) {
        return new RespBean(200, message, data);
    }

    public static RespBean error(String message) {
        return new RespBean(500, message, null);
    }

    public static RespBean error(String message, Object data) {
        return new RespBean(500, message, data);
    }
}
