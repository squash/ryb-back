package com.example.server.pojo;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

/**
 * websocket聊天消息对象
 *
 * @author zcg
 * @since 2021/9/20
 */
@Data
@Accessors(chain = true)
public class ChatMsg {

    private String from;
    private String to;
    private String content;

    private LocalDateTime date;

    private String fromNickname;

}
