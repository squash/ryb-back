package com.example.mail.consumer;

import com.example.server.pojo.Employee;
import com.example.server.pojo.MailConstants;
import com.rabbitmq.client.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.support.AmqpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.mail.MailProperties;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.Date;


/**
 * rabbit mq 消息消费者
 *
 * @author zcg
 * @since 2021/9/19
 */
@Component
public class MqMessageConsumer {

    public static final Logger LOGGER = LoggerFactory.getLogger(MqMessageConsumer.class);

    @Autowired
    private MailProperties mailProperties;

    @Autowired
    private TemplateEngine templateEngine;

    @Autowired
    private JavaMailSender javaMailSender;

    @Autowired
    private RedisTemplate redisTemplate;

    @RabbitListener(queues = MailConstants.MAIL_QUEUE_NAME)
    public void listenerMailMessage(Message<Employee> message, Channel channel) {
        Employee employee = message.getPayload();
        MessageHeaders headers = message.getHeaders();
        // 消息序号
        long tag = (Long) headers.get(AmqpHeaders.DELIVERY_TAG);
        String msgId = (String) headers.get("spring_returned_message_correlation");

        HashOperations hashOperations = redisTemplate.opsForHash();

        try {
            if (hashOperations.entries("mail_log").containsKey(msgId)) {
                LOGGER.error("邮件已经被消费================{}", msgId);

                // 手动确认一条
                channel.basicAck(tag, false);
                return;
            }

            MimeMessage msg = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(msg);
            // 发件人
            helper.setFrom(mailProperties.getUsername());
            // 收件人
            helper.setTo(employee.getEmail());
            // 主题
            helper.setSubject("入职欢迎邮件");
            // 发送日期
            helper.setSentDate(new Date());

            Context context = new Context();
            context.setVariable("name", employee.getName());
            context.setVariable("posName", employee.getPosition().getName());
            context.setVariable("joblevelName", employee.getJoblevel().getName());
            context.setVariable("departmentName", employee.getDepartment().getName());
            String mailContent = templateEngine.process("mail", context);
            helper.setText(mailContent, true);

            // 发送邮件
            javaMailSender.send(msg);

            LOGGER.info("邮箱发送成功============");
            // 将消息id存入redis
            hashOperations.put("mail_log", msgId, "OK");
            channel.basicAck(tag, false);
        } catch (MessagingException | IOException e) {
            try {
                // tag:消息序号，multiple:是否确认多条，requeue:是否退回到队列
                channel.basicNack(tag, false, true);
            } catch (IOException ioException) {
                LOGGER.error("邮件发送失败================{}", e.getMessage());
                ioException.printStackTrace();
            }
            LOGGER.error("邮件发送失败================{}", e.getMessage());
            e.printStackTrace();
        }
    }

}
